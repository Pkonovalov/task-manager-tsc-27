package ru.konovalov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exeption.entity.RoleNotFoundException;

import static ru.konovalov.tm.util.ValidationUtil.checkRole;

@Getter
public enum Role {
    USER("User"),
    ADMIN("Admin");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    public static @NotNull Role getRole(String s) {
        s = s.toUpperCase();
        if (!checkRole(s)) throw new RoleNotFoundException();
        return valueOf(s);
    }

}
