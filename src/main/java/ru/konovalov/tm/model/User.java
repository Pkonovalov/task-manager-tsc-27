package ru.konovalov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {

    @Nullable
    private String login;
    @Nullable
    private String passwordHash;
    @Nullable
    private String email;
    @Nullable
    private String firstName;
    @Nullable
    private String lastName;
    @Nullable
    private String middleName;
    @Nullable
    private Role role = Role.USER;
    @Nullable
    private boolean locked = false;

    public boolean isLocked() {
        return locked;
    }

}
