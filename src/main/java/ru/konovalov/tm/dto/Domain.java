package ru.konovalov.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.model.Project;
import ru.konovalov.tm.model.Task;
import ru.konovalov.tm.model.User;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter

public class Domain implements Serializable {

    @Nullable
    private List<Project> projects;

    @Nullable
    private List<Task> tasks;

    @Nullable
    private List<User> users;

    public void setUsers(@NotNull List<User> users) {
        this.users = users;
    }

    public void setProjects(@NotNull List<Project> projects) {
        this.projects = projects;
    }

    public void setTasks(@NotNull List<Task> tasks) {
        this.tasks = tasks;
    }
}
