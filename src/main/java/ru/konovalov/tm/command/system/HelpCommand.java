package ru.konovalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.command.AbstractCommand;

public final class HelpCommand extends AbstractCommand {

    @Override
    public @NotNull
    final String arg() {
        return "-h";
    }

    @Override
    public @NotNull
    final String name() {
        return "help";
    }

    @Override
    public @NotNull
    final String description() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (final AbstractCommand command : serviceLocator.getCommandService().getCommands()) {
            System.out.println(command.name() + ": " + command.description());
        }
    }
}