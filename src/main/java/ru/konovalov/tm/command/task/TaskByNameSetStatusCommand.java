package ru.konovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.entity.TaskNotFoundException;
import ru.konovalov.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskByNameSetStatusCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-set-status-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Set task status by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SET TASK STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        if (!serviceLocator.getTaskService().existsByName(userId, name)) throw new TaskNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        serviceLocator.getTaskService().changeTaskStatusByName(userId, name, Status.getStatus(TerminalUtil.nextLine()));
    }

}
